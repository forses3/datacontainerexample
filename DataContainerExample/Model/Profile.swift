//
//  Profile.swift
//  DataContainerExample
//
//  Created by Максим Ильин on 23.02.2021.
//

import Foundation

final class Profile: NSObject, ContainerValueInterface {

    var uniqueId: String {
        String(id)
    }
    
    var id: Int
    @objc dynamic var name: String
    
    init(id: Int, name: String) {
        self.id = id
        self.name = name
        super.init()
    }
    
    func update(with value: Profile) {
        self.name = value.name
    }
    
}
