//
//  ProfileViewController.swift
//  DataContainerExample
//
//  Created by Максим Ильин on 23.02.2021.
//

import UIKit

final class ProfileViewController: UIViewController {
    
    @IBOutlet private weak var profileNameLabel: UILabel!
    
    private var profileNameObservation: NSKeyValueObservation?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let profile = fetchProfileFromServer()
        profileNameLabel.text = profile.name
        
        profileNameObservation = profile.observe(\.name) { [weak self] (profile, _) in
            self?.profileNameLabel.text = profile.name
        }
    }
    
}

private extension ProfileViewController {
    
    func fetchProfileFromServer() -> Profile {
        /*
         Logic of fetching JSON from server, decode it...
         */
        
        let profile = Profile(id: 1, name: "Max")
        DataContainer.shared.setValue(Profile.self, value: profile)
        return profile
    }
    
}
