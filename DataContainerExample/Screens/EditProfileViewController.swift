//
//  EditProfileViewController.swift
//  DataContainerExample
//
//  Created by Максим Ильин on 23.02.2021.
//

import UIKit

final class EditProfileViewController: UIViewController {
    
    @IBOutlet private weak var changeButton: UIButton!
    
    private var iterator: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        changeButton.addTarget(self, action: #selector(handleChangeButtonTapped(_:)), for: .touchUpInside)
    }
    
    @objc private func handleChangeButtonTapped(_ button: UIButton) {
        guard let profile = DataContainer.shared.fetchValue(Profile.self, with: "1") else { return }
        iterator += 1
        profile.name = "Changed name \(iterator)"
    }
    
}
