//
//  ValueContainer.swift
//  DataContainerExample
//
//  Created by Максим Ильин on 23.02.2021.
//

import Foundation

protocol ContainerValueInterface: NSObject {
    var uniqueId: String { get }
    
    func update(with value: Self)
}

final class ValueContainer {
    
    private var container: [String: Any] = [:]
    
    func setValue<T: ContainerValueInterface>(_ value: T) {
        if let containerValue = container[value.uniqueId] as? T {
            containerValue.update(with: value)
        } else {
            container[value.uniqueId] = value
        }
        container.updateValue(value, forKey: value.uniqueId)
    }
    
    func fetchValue<T: ContainerValueInterface>(with id: String) -> T? {
        container[id] as? T
    }
    
    func removeValue(with id: String) {
        container.removeValue(forKey: id)
    }
    
    func removeAll() {
        container.removeAll()
    }
    
}
