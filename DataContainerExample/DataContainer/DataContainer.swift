//
//  DataContainer.swift
//  DataContainerExample
//
//  Created by Максим Ильин on 23.02.2021.
//

final class DataContainer {
    
    static let shared = DataContainer()
    
    private var container = [String: ValueContainer]()
    
    private init() { }
    
    func setValue<T: ContainerValueInterface>(_ type: T.Type, value: T) {
        let key = String(describing: type)
        let valueContainer: ValueContainer
        
        if let _valueContainer = container[key] {
            valueContainer = _valueContainer
        } else {
            let _valueContainer = ValueContainer()
            container.updateValue(_valueContainer, forKey: key)
            valueContainer = _valueContainer
        }
        
        valueContainer.setValue(value)
    }
    
    func fetchValue<T: ContainerValueInterface>(_ type: T.Type, with id: String) -> T? {
        let key = String(describing: type)
        guard let valueContainer = container[key] else { return nil }
        return valueContainer.fetchValue(with: id)
    }
    
    func removeValue<T: ContainerValueInterface>(_ type: T.Type, with id: String) {
        let key = String(describing: type)
        guard let valueContainer = container[key] else { return }
        valueContainer.removeValue(with: id)
    }
    
    func removeAll<T: ContainerValueInterface>(_ type: T.Type) {
        let key = String(describing: type)
        guard let valueContainer = container[key] else { return }
        valueContainer.removeAll()
    }
    
}
